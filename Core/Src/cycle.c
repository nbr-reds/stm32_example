/**
 ******************************************************************************
 * @file    cycle.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    07-June-2023
 * @brief   Count the instruction cycles
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include "main.h"

#include "cycle.h"

/* Public functions ----------------------------------------------------------*/

void cycle_init(void)
{
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    ITM->LAR = 0xC5ACCE55;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}

uint32_t cycle_get(void)
{
    return DWT->CYCCNT;
}
