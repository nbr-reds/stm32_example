/**
 ******************************************************************************
 * @file    button.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    05-June-2023
 * @brief   Button driver
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef BUTTON_H
#define BUTTON_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the button driver
 */
void button_init(void);

/**
 * Get the state of the button
 * @param the state of the button
 */
bool button_get(void);

/**
 * Set the interrupt callback
 * @param callback the interrupt callback
 */
void button_set_irq_callback(void (*callback)(void));

#endif
