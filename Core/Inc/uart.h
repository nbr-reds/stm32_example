/**
 ******************************************************************************
 * @file    uart.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    05-June-2023
 * @brief   UART driver
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef UART_H
#define UART_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the UART driver
 */
void uart_init(void);

/**
 * Write data to the UART
 * @param data the data to be written
 * @param size the number of byte to be written
 */
void uart_write(const uint8_t* data, uint16_t size);


/**
 * Read data from the UART
 * @param data the data to be written
 * @param size the number of byte to be read
 * @param timeout the timeout in tick
 */
void uart_read(uint8_t* data, uint16_t size, uint32_t timeout);

#endif
