/**
 ******************************************************************************
 * @file    cycle.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    07-June-2023
 * @brief   Count the instruction cycles
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef CYCLE_H
#define CYCLE_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the cycle counter
 */
void cycle_init(void);

/**
 * Get the value of the cycle counter
 * @return the value of the cycle counter
 */
uint32_t cycle_get(void);

#endif
