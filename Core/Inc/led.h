/**
 ******************************************************************************
 * @file    led.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    05-June-2023
 * @brief   LED driver
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef LED_H
#define LED_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

/* Exported types ------------------------------------------------------------*/

typedef enum {
    LED_OFF,
    LED_ON,
    LED_BLINK,
} led_state_t;

/* Exported functions --------------------------------------------------------*/

/**
 * Initialize the LED driver
 */
void led_init(void);

/**
 * Set the LED state
 * @param state the state of the LED
 */
void led_set(led_state_t state);

#endif
