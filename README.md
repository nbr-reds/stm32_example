# stm32_example
Code example for STM32CubeIDE using GPIO, UART and a timer on a NUCLEO-L476RG. The documentation concerning this project can be found [here](https://gitlab.com/stm32-walkthrough/doc/-/raw/main/walkthrough/stm32_walkthrough.pdf).
